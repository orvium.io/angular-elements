// import { bootstrapApplication } from '@angular/platform-browser';
// import { appConfig } from './app/app.config';
// import { AppComponent } from './app/app.component';

// bootstrapApplication(AppComponent, appConfig)
//   .catch((err) => console.error(err));


import { createCustomElement } from "@angular/elements";
import { createApplication } from "@angular/platform-browser";
import { MyWebcomponentComponent } from "./app/my-webcomponent/my-webcomponent.component";

(async () => {
  const app = await createApplication({
    providers: [],
  });

  const myComponent = createCustomElement(MyWebcomponentComponent, {
    injector: app.injector,
  });
  customElements.define('my-webcomponent', myComponent);
})();
