import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyWebcomponentComponent } from './my-webcomponent.component';

describe('MyWebcomponentComponent', () => {
  let component: MyWebcomponentComponent;
  let fixture: ComponentFixture<MyWebcomponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MyWebcomponentComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MyWebcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
