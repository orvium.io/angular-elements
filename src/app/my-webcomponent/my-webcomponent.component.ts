import { Component } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';

@Component({
  selector: 'app-my-webcomponent',
  standalone: true,
  imports: [MatCardModule, MatButtonModule],
  templateUrl: './my-webcomponent.component.html',
  styleUrl: './my-webcomponent.component.scss'
})
export class MyWebcomponentComponent {

}
