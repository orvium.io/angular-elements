import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { MyWebcomponentComponent } from "./my-webcomponent/my-webcomponent.component";

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet, MyWebcomponentComponent]
})
export class AppComponent {
  title = 'angular-elements';
}
